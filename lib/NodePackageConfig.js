/*
global require, process, module
*/

// Global methods
var joinPath = require('path').join;

// Global constants
var FILE = require('./GulpConst').FILE;
var DIR = require('./GulpConst').DIR;

// Global classes
var NodeSassString = require('node-sass').types.String;

var cheerioConfig = {
    lowerCaseAttributeNames: false,
    decodeEntities: false
};

var sassConfig = {
    functions: {
        'inlineSvg($string)': function($string) {
            var buffer = new Buffer( $string.getValue() );
            return NodeSassString(['data:image/svg+xml;base64', buffer.toString('base64')].join(','));
        }
    }
};

var systemJsConfig = require( joinPath(__dirname, '..', FILE.SYSTEM_JS_CONFIG) );

var serverConfig = {
    port: 8081,
    server: {
        baseDir: DIR.DIST
    },
    ui: false,
    open: true
};

var watchConfig = {ignoreInitial: true};

module.exports = {
    cheerioConfig: cheerioConfig,
    sassConfig: sassConfig,
    systemJsConfig: systemJsConfig,
    serverConfig: serverConfig,
    watchConfig: watchConfig
};
