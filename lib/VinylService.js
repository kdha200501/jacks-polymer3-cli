/*
global require, module
*/

// Global methods
var joinPath = require('path').join;
var relativePath = require('path').relative;
var dirname = require('path').dirname;
var basename = require('path').basename;
var cloneDeep = require('lodash.clonedeep');
var safeAccess = require('safe-access');

// Global constants
var ENV = require('./GulpConst').ENV;
var FILE = require('./GulpConst').FILE;
var DIR = require('./GulpConst').DIR;
var NodePackageConfig = require('./NodePackageConfig');

// Global classes
var NodePackageService = require('./NodePackageService');
var PolyfillFactory = require('./PolyfillFactory');
var LogService = require('./LogService');
var PolymerTemplateInjection = require('./PolymerTemplateInjection');
var Cheerio = require('cheerio');
var Builder = require('systemjs-builder');
var URL = require('url').URL;

var VinylService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageService = NodePackageService.getInstance();



  var vinylMethodList = [{

    name: 'injectTemplate',
    method: function(vinyl, next) {
        var polymerTemplateInjection = new PolymerTemplateInjection();
        vinyl.contents = new Buffer(
            polymerTemplateInjection( vinyl.contents.toString(), vinyl.path )
        );
        next(null, vinyl);
    }

  }, {

    name: 'bundleLibrary',
    method: function(vinyl, next) {
        var globSrc = joinPath(vinyl.path, '**', '*');
        var bundleArithmetic = `${globSrc} - [${globSrc}]`;

        var builder = new Builder(NodePackageConfig.systemJsConfig);

        builder.bundle(bundleArithmetic, {minify: true})
            .then(function(output) {
                vinyl.contents = new Buffer(output.source);
                vinyl.basename = FILE.BUNDLE_LIB;

                LogService.info.call('bundleLibrary', 'finishes bundling library');
                next(null, vinyl);
            })
            .catch(function(e) {
                LogService.error.call('bundleLibrary', 'encounters error when bundling library');
                LogService.error.call('bundleLibrary', 'error:', e);
                next();
            });
    }

  }, {

    name: 'bundleSource',
    method: function(vinyl, next) {
        var builder = new Builder(NodePackageConfig.systemJsConfig);
        var reg = new RegExp( DIR.NODE_MODULES );
        var polymerTemplateInjection = new PolymerTemplateInjection();

        var bundleArithmetic = `${vinyl.path} - ${joinPath(DIR.CWD, DIR.DIST, FILE.BUNDLE_LIB)}`;

        builder.bundle(bundleArithmetic, {
            minify: true,
            sourceMaps: true,
            fetch: function(load, fetch) {
                if( !reg.test(load.address) ) {
                    var path = new URL(load.name).pathname;
                    var content = nodePackageService.readFile(path);
                    return polymerTemplateInjection(content.toString(), path);
                }
                //endIf imported file is part of the project code base
                else {
                    return fetch(load);
                }
                //endIf imported file is outside of the project code base
            }
            // endFetch imported file
        })
            .then(function(output) {
                var mapRelativePath = relativePath( joinPath(DIR.CWD, DIR.SRC), vinyl.path );
                mapRelativePath = joinPath(DIR.DIST, mapRelativePath);
                mapRelativePath = [mapRelativePath, 'map'].join('.');
                nodePackageService.writeFile(mapRelativePath, output.sourceMap);

                vinyl.contents = new Buffer(output.source);

                LogService.info.call('bundleSource', 'finishes bundling for module:', vinyl.path);
                next(null, vinyl);
            })
            .catch(function(e) {
                LogService.error.call('bundleSource', 'encounters error when processing module:', vinyl.path);
                LogService.error.call('bundleSource', 'error:', e);
                next();
            });
    }

  }, {

      name: 'injectHead',
      method: function(vinyl, next) {
          var isProd = nodePackageService.getArgs()[ENV.PROD];
          var $ = Cheerio.load(vinyl.contents, NodePackageConfig.cheerioConfig);

          PolyfillFactory.instantiate(isProd ? ENV.PROD : ENV.DEV)
              .reduce(PolyfillFactory.resolvePath, [])
              .forEach(function(polyfill) {
                  $('head').append( '\n', Cheerio('<script>').attr('src', polyfill) );
              });
          if(isProd) {
              var libRelativePath = joinPath('..', FILE.BUNDLE_LIB);
              $('head').append( '\n', Cheerio('<script>').attr('src', libRelativePath) );
          }
          $('head').append( '\n', Cheerio('<script>').attr('src', FILE.SYSTEM_JS_LOADER) );

          vinyl.contents = Buffer.from( $.html() );
          next(null, vinyl);
      }

  }, {

      name: 'addModuleImportToSystemJsLoader',
      method: function(vinyl, next) {
          var modulePath = relativePath( joinPath(DIR.CWD, DIR.SRC), vinyl.path );
          modulePath = joinPath(DIR.SRC, modulePath);

          var systemJsConfig = cloneDeep(NodePackageConfig.systemJsConfig);
          systemJsConfig['paths'][`${dirname(modulePath)}/`] = '';// TODO: find out if this works in Windows

          var lazyLoadingPath = joinPath( dirname(vinyl.path), FILE.LAZYLOADING );
          if( nodePackageService.fileExists(lazyLoadingPath) ) {
              var importExpressionList = safeAccess(
                  nodePackageService.parseJs(nodePackageService.readFile(lazyLoadingPath)),
                  'body'
              ) || [];

              var systemJsImportList = importExpressionList
                  .filter(function(importExpression) {
                      return safeAccess(importExpression, 'expression.callee.property.name') === 'import';
                  })
                  .map(function(importExpression) {
                      var pathRelativeToSrc = joinPath(
                          dirname(vinyl.path), safeAccess(importExpression, 'expression.arguments[0].value')
                      );
                      pathRelativeToSrc = relativePath(DIR.CWD, pathRelativeToSrc);
                      var namespaceDir = dirname(pathRelativeToSrc);
                      var artifactDir = dirname( safeAccess(importExpression, 'expression.arguments[0].value') );
                      // maps an importee artifact's System Js namespace directory to the importee artifacts's relative directory
                      systemJsConfig['paths'][namespaceDir] = artifactDir;
                      // instruct the importee artifact to be downloaded prior to loading the module
                      return `SystemJS.import('${pathRelativeToSrc}')`;
                  });
                  //endEach import statement

              vinyl.contents = Buffer.from([
                  `SystemJS.config(${JSON.stringify(systemJsConfig)});`,
                  `Promise.all([${systemJsImportList.join(',')}]).then(function() {SystemJS.import("${FILE.WEB_COMPONENT_MODULE}");});`
              ].join('\n'));
          }
          //endIf the module has dynamic System Js import
          else {
              var isProd = nodePackageService.getArgs()[ENV.PROD];
              vinyl.contents = Buffer.from([
                  `SystemJS.config(${JSON.stringify(systemJsConfig)});`,
                  `SystemJS.import("${ isProd ? modulePath : FILE.WEB_COMPONENT_MODULE }");`
              ].join('\n'));
          }
          //endIf the module does not have dynamic System Js import

          vinyl.basename = FILE.SYSTEM_JS_LOADER;
          next(null, vinyl);
      }

  }, {

      name: 'compileSass',
      method: function(vinyl, next) {
          var sassConfig = cloneDeep(NodePackageConfig.sassConfig);
          sassConfig['file'] = vinyl.path;

          var isProd = nodePackageService.getArgs()[ENV.PROD];
          if(isProd) {
              sassConfig['outputStyle'] = 'compressed';// uglify css
          }

          var result = nodePackageService.compileSass(sassConfig);
          vinyl.contents = Buffer.from(result.css);
          vinyl.basename = [basename(vinyl.path, 'scss'), 'css'].join('');
          next(null, vinyl);
      }

  }];

    // private static class
    var VinylService_ = function(config) {
        return function(vinylMethodList) {// closure

            // private variables and methods
            var buildService = function(service, vinylMethod) {
                service[vinylMethod.name] = vinylMethod.method;
                return service;
            };

            // public methods
            return vinylMethodList.reduce(buildService, {});

        }(config.vinylMethodList || []);// endClosure
    };

    // public singleton
    var singleton = null;
    return {
        getInstance: function() {
            if(singleton === null) {
                singleton = new VinylService_({vinylMethodList: vinylMethodList});
            }
            return singleton;
        }
    };
}();// endClosure

module.exports = VinylService;
