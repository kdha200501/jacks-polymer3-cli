/*
global require, module
*/

// Global Classes
var NodePackageService = require('./NodePackageService');

var BrowserService = function() {// closure
  // private variables and methods
  var nodePackageService = NodePackageService.getInstance();

  var locked = false;
  var debounce = 100;// ms

  var debouncedReload = function() {
    if(locked === true) {
      return;
    }
    locked = true;
    setTimeout(function() {
      locked = false;
      nodePackageService.reloadBrowser();
    }, debounce);
  };

  // public methods
  return {
    reload: debouncedReload
  }
}();// endClosure

module.exports = BrowserService;
