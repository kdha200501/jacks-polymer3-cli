/*
global require, module
*/

// Global methods
var gulp = require('gulp4');
var registerTask = require('gulp4').task;
var joinPath = require('path').join;
var relativePath = require('path').relative;
var dirname = require('path').dirname;
var listDir = require('fs').readdirSync;
var gulpIf = require('gulp-if');
var map = require('map-stream');
var merge = require('merge-stream');

// Global constants
var NodePackageConfig = require('./NodePackageConfig');
var DIR = require('./GulpConst').DIR;
var FILE = require('./GulpConst').FILE;
var ENV = require('./GulpConst').ENV;

// Global classes
var NodePackageService = require('./NodePackageService');
var VinylService = require('./VinylService');
var BrowserService = require('./BrowserService');
var LogService = require('./LogService');

/*
    note:
        gulp.src() works with paths relative to process.cwd()
        require() works with paths relative to this file's __dirname
*/

var GulpTaskService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageService = NodePackageService.getInstance();
  var vinylService = VinylService.getInstance();

  var gulpTaskList = [{

    name: 'distRmRF',
    method: function() {
      return nodePackageService.rmRF(DIR.DIST);
    }

  }, {

    name: 'distMkdirP',
    method: function(next) {
      return nodePackageService.mkdirP(DIR.DIST, function(err) {
        if(err === null) {
          next();
        }
        else {
          throw err;
        }
      });
    }

  }, {

      name: 'copySource',
      method: function() {
          var src = gulp.src([
              joinPath(DIR.SRC, '*', '**', '*.js'),
              `!${joinPath(DIR.SRC, '*', FILE.LAZYLOADING)}`
          ], {allowEmpty: true});
          var dst = gulp.dest(DIR.DIST);
          return src
              .pipe( map(vinylService.injectTemplate) )
              .pipe(dst);
      }

  }, {

      name: 'bundleLibrary',
      method: function() {
          var src = gulp.src(DIR.SRC, {allowEmpty: true});
          var dst = gulp.dest(DIR.DIST);
          return src
              .pipe( map(vinylService.bundleLibrary) )
              .pipe(dst);
      }

  }, {

      name: 'bundleSource',
      method: function() {
          var src = gulp.src( joinPath(DIR.SRC, '*', '**', 'Module.js'), {allowEmpty: true} );
          var dst = gulp.dest(DIR.DIST);
          return src
              .pipe( map(vinylService.bundleSource) )
              .pipe(dst);
      }

  }, {

      name: 'copyTemplate',
      method: function() {
          var src = gulp.src( joinPath(DIR.SRC, '*', '*.html'), {allowEmpty: true} );
          var dst = gulp.dest(DIR.DIST);
          return src
              .pipe( map(vinylService.injectHead) )
              .pipe(dst)
      }

  }, {

      name: 'copySystemJsConfig',
      method: function() {
          var src = gulp.src( joinPath(DIR.SRC, '*', FILE.WEB_COMPONENT_MODULE), {allowEmpty: true} );
          var dst = gulp.dest(DIR.DIST);
          return src
              .pipe( map(vinylService.addModuleImportToSystemJsLoader) )
              .pipe(dst);
      }

  }, {

      name: 'copyShadowDomStyle',
      method: function() {

          var src = gulp.src( joinPath(DIR.SRC, '*', '*.scss'), {allowEmpty: true} );
          var dst = gulp.dest(DIR.DIST);

          return src
              .pipe( map(vinylService.compileSass) )
              .pipe(dst);
       }

  }, {

      name: 'linkNodeModules',
      method: function() {
          var pipeList = nodePackageService.ls(DIR.SRC)
              .filter(function(name) {
                  return name.toLowerCase() !== '.ds_store';
              })
              .map(function(name) {
                  return joinPath(DIR.SRC, name)
              })
              .filter(function(path) {
                  return nodePackageService.lstat(path).isDirectory
              })
              .map(function(path) {// TODO: replace this with https://www.npmjs.com/package/gulp-multi-dest
                  var src = gulp.src(DIR.NODE_MODULES, {read: false, allowEmpty: true});
                  var dst = gulp.symlink( joinPath( DIR.DIST, relativePath(DIR.SRC, path) ) );
                  return src.pipe(dst);
              })
          return merge.apply(null, pipeList);
      }

  }, {

      name: 'startServer',
      method: function(next, logInfo) {
          nodePackageService.startServer(NodePackageConfig.serverConfig, function(err) {
              if(err) {
                  throw err;
              }
              next();
          });
      }

  }, {

      name: 'watchSource',
      method: function(next, logInfo) {

          gulp.watch([
              joinPath(DIR.SRC, '*', '*', '*.{js,html,scss}'),
              joinPath(DIR.SRC, '*', '*.js')
          ], NodePackageConfig.watchConfig)
              .on('change', function(path) {
                  logInfo('file changed:', path);
                  var srcDirPath = dirname(path);
                  var dstDirPath = joinPath( DIR.DIST, relativePath(DIR.SRC, srcDirPath) );
                  var src = gulp.src([
                      joinPath(srcDirPath, '*.js'),
                      `!${joinPath(DIR.SRC, '*', FILE.LAZYLOADING)}`
                  ], {allowEmpty: true});
                  var dst = gulp.dest(dstDirPath);
                  src
                      .pipe( map(vinylService.injectTemplate) )
                      .pipe(dst)
                      .pipe( map(BrowserService.reload) );
              });
      }

  }, {

      name: 'watchTemplate',
      method: function(next, logInfo) {

          gulp.watch( joinPath(DIR.SRC, '*', '*.html'), NodePackageConfig.watchConfig )
              .on('change', function(path) {
                  logInfo('file changed:', path);
                  var src = gulp.src(path);
                  var dst = gulp.dest( joinPath( DIR.DIST, relativePath(DIR.SRC, dirname(path)) ) );
                  src
                      .pipe( map(vinylService.injectHead) )
                      .pipe(dst)
                      .pipe( map(BrowserService.reload) );
              });
      }

  }, {

      name: 'watchLazyloadingConfig',
      method: function(next, logInfo) {

          gulp.watch( joinPath(DIR.SRC, '*', FILE.LAZYLOADING), NodePackageConfig.watchConfig )
              .on('change', function(path) {
                  logInfo('file changed:', path);
                  var src = gulp.src(path, {allowEmpty: true});
                  var dst = gulp.dest( joinPath( DIR.DIST, relativePath(DIR.SRC, dirname(path)) ) );
                  return src
                      .pipe( map(vinylService.addModuleImportToSystemJsLoader) )
                      .pipe(dst)
                      .pipe( map(BrowserService.reload) );
              });
      }

  }, {

      name: 'watchShadowDomStyleAndMixin',
      method: function(next, logInfo) {

          gulp.watch( joinPath(DIR.SRC, '*', '*.scss'), NodePackageConfig.watchConfig )
              .on('change', function(path) {
                  logInfo('file changed:', path);
                  var srcSource = gulp.src([
                      joinPath(DIR.SRC, '**', '*.js'),
                      `!${joinPath(DIR.SRC, '*', FILE.LAZYLOADING)}`
                  ], {allowEmpty: true});
                  var dstSource = gulp.dest(DIR.DIST);
                  srcSource
                      .pipe( map(vinylService.injectTemplate) )
                      .pipe(dstSource);

                  var srcStyle = gulp.src(path, {allowEmpty: true});
                  var dstStyle = gulp.dest( joinPath( DIR.DIST, relativePath(DIR.SRC, dirname(path)) ) );
                  srcStyle
                      .pipe( map(vinylService.compileSass) )
                      .pipe(dstStyle)
                      .pipe( map(BrowserService.reload) );
              });
      }

  }, {

    name: 'noop',
    method: function(next) {
      next();
    }

  }];

  // private static class
  var singleton = null;
  var GulpTaskService_ = function(config) {
    return function(gulpTaskList) {// closure

      // private variables and methods
      var buildService = function(service, gulpTask) {
        var logInfo = function() {// prepended info log with task name
          LogService.info.apply(gulpTask.name, arguments);
        };
        var logError = function() {// prepended error log with task name
          LogService.error.apply(gulpTask.name, arguments);
        };
        var method = function(next) {
          if(gulpTask.name !== 'noop') {
            logInfo('Started');
          }
          return gulpTask.method(next, logInfo, logError);
        };
        registerTask(gulpTask.name, method);
        service[gulpTask.name] = method;
        return service;
      };

      // public methods
      return gulpTaskList.reduce(buildService, {});

    }(config.gulpTaskList || []);// endClosure
  };

  // public singleton
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new GulpTaskService_({gulpTaskList: gulpTaskList});
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = GulpTaskService;
