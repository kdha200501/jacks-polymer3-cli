/*
global require, module
*/

// Global methods
var safeAccess = require('safe-access');

// Global variables
var ENV = require('./GulpConst').ENV;

var NodePackageService = function() {// closure
  'use strict';

  // private static variables
  var nodePackageList = [{
    name: 'yargs',
    config: function(nodePackage) {
      nodePackage.boolean([// force args to bool
        // environment flags
        ENV.DEV, ENV.PROD,
        // build specific flags
        'serve'
      ]);
    },
    method: 'argv',
    serviceMethod: 'getArgs'
  }, {
    name: 'fs-extra',
    method: 'mkdirs()',
    serviceMethod: 'mkdirP'
  }, {
    name: 'fs-extra',
    method: 'copy()',
    serviceMethod: 'cpR'
  }, {
    name: 'fs-extra',
    method: 'readJsonSync()',
    serviceMethod: 'readJsonFile'
  }, {
    name: 'del',
    serviceMethod: 'rmRF'
  }, {
    name: 'del',
    serviceMethod: 'rm'
  }, {
    name: 'browser-sync',
    config: function(nodePackage) {
      nodePackage.create();
    },
    method: 'init()',
    serviceMethod: 'startServer'
  }, {
    name: 'browser-sync',
    method: 'reload()',
    serviceMethod: 'reloadBrowser'
  }, {
    name: 'openurl',
    method: 'open()',
    serviceMethod: 'openUrl'
  }, {
    name: 'fs',
    method: 'readdirSync()',
    serviceMethod: 'ls'
  }, {
    name: 'fs',
    method: 'lstatSync()',
    serviceMethod: 'lstat'
  }, {
    name: 'fs',
    method: 'lstatSync()',
    serviceMethod: 'fileProperty'
  }, {
    name: 'fs',
    method: 'readFileSync()',
    serviceMethod: 'readFile'
  }, {
    name: 'fs',
    method: 'writeFileSync()',
    serviceMethod: 'writeFile'
  }, {
    name: 'file-exists',
    method: 'sync()',
    serviceMethod: 'fileExists'
  }, {
    name: 'gulp-sourcemaps',
    method: 'init()',
    serviceMethod: 'createSourceMap'
  }, {
    name: 'gulp-sourcemaps',
    method: 'write()',
    serviceMethod: 'writeSourceMap'
  }, {
    name: 'url',
    method: 'parse()',
    serviceMethod: 'parseUrl'
  }, {
    name: 'node-sass',
    method: 'renderSync()',
    serviceMethod: 'compileSass'
  }, {
    name: 'esprima',
    method: 'parse()',
    serviceMethod: 'parseJs'
  }, {
    name: 'child_process',
    method: 'exec()',
    serviceMethod: 'eval'
  }];

  // private static class
  var singleton = null;
  var NodePackageService_ = function(config) {
    return function(nodePackageList) {// closure

      // private variables and methods
      var buildService = function(service, nodePackage) {
        var node = require(nodePackage.name);
        // 1. config node package
        if( (typeof nodePackage.config).toLowerCase() === 'function' ) {
          nodePackage.config(node);
        }
        // 2. expose node package method
        if( (typeof nodePackage.method).toLowerCase() === 'string' ) {
          service[nodePackage.serviceMethod] = function() {
            return safeAccess( node, nodePackage.method, Array.prototype.slice.call(arguments, 0) );
          };
        }
        else {
          service[nodePackage.serviceMethod] = node;
        }
        return service;
      };

      // public methods
      return nodePackageList.reduce(buildService, {});

    }(config.nodePackageList || []);// endClosure
  };

  // public singleton
  return {
    getInstance: function() {
      if(singleton === null) {
        singleton = new NodePackageService_({nodePackageList: nodePackageList});
      }
      return singleton;
    }
  };
}();// endClosure

module.exports = NodePackageService;
