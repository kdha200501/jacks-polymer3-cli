/*
global require, process, module
*/

// Global methods
var joinPath = require('path').join;


var DIR = {
  CWD: process.cwd(),
  DIST: 'dist',// build
  NODE_MODULES: 'node_modules',
  SRC: 'src'// ES6 Javascript source code
};

var FILE = {
  WEB_COMPONENT_MODULE: 'Module.js',
  SYSTEM_JS_CONFIG: 'config.systemjs.json',
  SYSTEM_JS_LOADER: 'config.systemjs.js',
  LAZYLOADING: 'config.lazyloading.js',
  BUNDLE_LIB: 'lib.js'
};

var TASK = {
  DEFAULT: 'default',
  BUILD: 'build'
};

var ENV = {
  DEV: 'dev',
  PROD: 'prod'
};

module.exports = {
  DIR: DIR,
  FILE: FILE,
  TASK: TASK,
  ENV: ENV
};
