/*
global require, module
*/

// Global methods
var joinPath = require('path').join;
var dirname = require('path').dirname;
var parseJson = require('loose-json');
var cloneDeep = require('lodash.clonedeep');

// Global constants
var NodePackageConfig = require('./NodePackageConfig');
var ENV = require('./GulpConst').ENV;

// Global classes
var NodePackageService = require('./NodePackageService');
var Cheerio = require('cheerio');

var PolymerTemplateInjection = function() {
    // private static variables
    var templateRegExp = /html`{(.*)}`/sm;

    // private static methods
    var extractTemplateConfig = function(content, pathDir) {
        var jsonStr = ['{', content.match(templateRegExp)[1], '}'].join('');
        try {
            var templateConfig = parseJson(jsonStr);
            return {
                templatePath: joinPath(pathDir, templateConfig.templateUrl),
                stylePath: joinPath(pathDir, templateConfig.styleUrl)
            };
        }
        catch(e) {
            return null;
        }
    };

    return function() {// closure
        // private variables
        var nodePackageService = NodePackageService.getInstance();

        // public method
        return function(content, jsFilePath) {
            if( templateRegExp.test(content) ) {
                var templateConfig = extractTemplateConfig( content, dirname(jsFilePath) );

                if( nodePackageService.fileExists(templateConfig.templatePath) ) {
                    var $ = Cheerio.load(
                        nodePackageService.readFile(templateConfig.templatePath),
                        NodePackageConfig.cheerioConfig
                    );

                    if( nodePackageService.fileExists(templateConfig.stylePath) ) {
                        // Compile sass to css
                        var sassConfig = cloneDeep(NodePackageConfig.sassConfig);
                        sassConfig['file'] = templateConfig.stylePath;
                        var isProd = nodePackageService.getArgs()[ENV.PROD];
                        if(isProd) {
                            sassConfig['outputStyle'] = 'compressed';// uglify css
                        }
                        var result = nodePackageService.compileSass(sassConfig);

                        // Inject css to html
                        $(':root').first().before( '\n', Cheerio('<style>').text(result.css) );
                    }
                    //endIf style file exists

                    // Inject html to Web Component class
                    content = content.replace(templateRegExp, `html\`${$.html()}\``);
                }
                //endIf template file exists
            }
            //endIf needs template injection

            return content;
        }
    }();// endClosure
};

module.exports = PolymerTemplateInjection;
