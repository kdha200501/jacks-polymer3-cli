/*
global require, module
*/

// Global methods
var joinPath = require('path').join;

// Global variables
var DIR = require('./GulpConst').DIR;
var ENV = require('./GulpConst').ENV;
var TASK = require('./GulpConst').TASK;

// maps Es2015 packages' import path to file path
var PolyfillFactory = function() {// closure
    'use strict';

    // private variables and methods
    var sharedModuleList = [{
        name: 'babel-polyfill',
        path: 'babel-polyfill/dist/polyfill.min.js'
    }, {
        name: 'url-polyfill',
        path: '@publica/url-polyfill/url.js'
    }, {
        name: 'webcomponents-loader',
        path: '@webcomponents/webcomponentsjs/webcomponents-loader.js'
    }, {
        name: 'custom-elements-es5-adapter',
        path: '@webcomponents/webcomponentsjs/custom-elements-es5-adapter.js'
    }, {
        name: 'systemjs',
        path: 'systemjs/dist/system.js'
    }];

    var unbundledModuleList = [].concat(// System Js + Babel polyfills
        sharedModuleList,
        []
    );

    var bundledModuleList = [].concat(// ES5 bundling polyfills
        sharedModuleList,
        []
    );

    // public methods
    return {
        instantiate: function(env) {
            switch (env) {
                case ENV.DEV:

                    return unbundledModuleList;

                    break;
                case ENV.PROD:

                    return bundledModuleList;

                    break;
                default:

                    return [];
            }
        },
        resolvePath: function(op, module) {
            op.push( joinPath(DIR.NODE_MODULES, module.path) );
            return op;
        }
    };
}();// endClosure

module.exports = PolyfillFactory;
